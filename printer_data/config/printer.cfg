# This file contains pin mappings for the LDO Kit using BigTreeTech Octopus V1 as the main controller.
# To use this config, the firmware should be compiled for the STM32F446 with a "32KiB bootloader"
# Enable "extra low-level configuration options" and select the "12MHz crystal" as clock reference

# after running "make", copy the generated "klipper/out/klipper.bin" file to a
# file named "firmware.bin" on an SD card and then restart the Octopus with that SD card.

[include sensorless_homing.cfg]
[include toolhead_leds.cfg]
[include mainsail.cfg]
[include sensorless_adjust.cfg]

# See docs/Config_Reference.md for a description of parameters.

## Voron Design VORON2 250/300/350mm BigTreeTech Octopus V1 TMC2209 UART config

## *** THINGS TO CHANGE/CHECK: ***
## MCU paths                            [mcu] section
## Thermistor types                     [extruder] and [heater_bed] sections - See 'sensor types' list at end of file
## Z Endstop Switch location            [safe_z_home] section
## Homing end position                  [gcode_macro G32] section
## Z Endstop Switch  offset for Z0      [stepper_z] section
## Probe points                         [quad_gantry_level] section
## Min & Max gantry corner postions     [quad_gantry_level] section
## PID tune                             [extruder] and [heater_bed] sections
## Thermistor types                     [extruder] and [heater_bed] sections
## Probe pin                            [probe] section
## Fine tune E steps                    [extruder] section

[mcu]
##  Obtain definition by "ls -l /dev/serial/by-id/" then unplug to verify
##--------------------------------------------------------------------
canbus_uuid: 1c1370f598af     # BTT Octopus

# [mcu EBBCan]
# canbus_uuid: 4b8454cf9234   # t0 EBB36
# canbus_uuid: b239b15f3e0d   # t1 EBB36

##--------------------------------------------------------------------
[printer]
kinematics: corexy
max_velocity: 500
max_accel: 7000
max_z_velocity: 125         #Max 15 for 12V TMC Drivers, can increase for 24V
max_z_accel: 350
square_corner_velocity: 5.0

[input_shaper]
#shaper_type_x = 2hump_ei
#shaper_freq_x = 77.0
#shaper_type_y = mzv
#shaper_freq_y = 33.8

[force_move]
enable_force_move: True		# enable FORCE_MOVE and SET_KINEMATIC_POSITION

[rounded_path]
resolution: 0.2 # the length of a circle approximation segments.
replace_g0: False # Use at your own risk

# I like to make a symlink from the tapchanger repo to tapchanger in the
# config directory so I include them like this.
[include tapchanger/macros.cfg]
[include tapchanger/homing.cfg] # might need to modify, assumes sensorless
[include tapchanger/tool_detection.cfg]
[include tapchanger/toolchanger-2.4.cfg]

# Include whatever you called the configs for each of your tools.
[include Toolhead_T0.cfg]
[include Toolhead_T1.cfg]

#####################################################################
#   X/Y Stepper Settings
#####################################################################

##  B Stepper - Left
##  Connected to MOTOR0
##  Endstop connected to DIAG0
[stepper_x]
step_pin: PF13
dir_pin: PF12
enable_pin: !PF14
rotation_distance: 40
microsteps: 32
full_steps_per_rotation:400  #set to 200 for 1.8 degree stepper
endstop_pin: tmc2209_stepper_x:virtual_endstop
position_min: 0
##--------------------------------------------------------------------

##  Uncomment for 300mm build
position_endstop: 300
position_max: 300

##--------------------------------------------------------------------
homing_speed: 20   #Max 100
homing_retract_dist: 0
homing_positive_dir: true

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_x]
diag_pin: ^PG6
driver_SGTHRS: 101  # 255 is most sensitive value, 0 is least sensitive 90+(98-99)/3=2.666
uart_pin: PC4
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

##  A Stepper - Right
##  Connected to MOTOR1
##  Endstop connected to DIAG1
[stepper_y]
step_pin: PG0
dir_pin: PG1
enable_pin: !PF15
rotation_distance: 40
microsteps: 32
full_steps_per_rotation:400  #set to 200 for 1.8 degree stepper
endstop_pin: tmc2209_stepper_y:virtual_endstop
position_min: -10
##--------------------------------------------------------------------

##  Uncomment for 300mm build
position_endstop: 300
position_max: 300

##--------------------------------------------------------------------
homing_speed: 25  #Max 100
homing_retract_dist: 0
homing_positive_dir: true

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_y]
diag_pin: ^PG9
driver_SGTHRS: 100  # 255 is most sensitive value, 0 is least sensitive 100+(115-100)/3=105
uart_pin: PD11
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

#####################################################################
#   Z Stepper Settings
#####################################################################

## Z0 Stepper - Front Left
##  Connected to MOTOR2_1
##  Endstop connected to DIAG_2
[stepper_z]
step_pin: PF11
dir_pin: PG3
enable_pin: !PG5
rotation_distance: 40
gear_ratio: 80:16
microsteps: 32
endstop_pin: probe:z_virtual_endstop
##  Z-position of nozzle (in mm) to z-endstop trigger point relative to print surface (Z0)
##  (+) value = endstop above Z0, (-) value = endstop below
##  Increasing position_endstop brings nozzle closer to the bed
##  After you run Z_ENDSTOP_CALIBRATE, position_endstop will be stored at the very end of your config
#position_endstop: -0.5
##--------------------------------------------------------------------

##  Uncomment below for 300mm build
position_max: 300

##--------------------------------------------------------------------
position_min: -5
homing_speed: 8
second_homing_speed: 3
homing_retract_dist: 3

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_z]
uart_pin: PC6
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

##  Z1 Stepper - Rear Left
##  Connected to MOTOR3
[stepper_z1]
step_pin: PG4
dir_pin: !PC1
enable_pin: !PA0
rotation_distance: 40
gear_ratio: 80:16
microsteps: 32

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_z1]
uart_pin: PC7
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

##  Z2 Stepper - Rear Right
##  Connected to MOTOR4
[stepper_z2]
step_pin: PF9
dir_pin: PF10
enable_pin: !PG2
rotation_distance: 40
gear_ratio: 80:16
microsteps: 32

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_z2]
uart_pin: PF2
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

##  Z3 Stepper - Front Right
##  Connected to MOTOR5
[stepper_z3]
step_pin: PC13
dir_pin: !PF0
enable_pin: !PF1
rotation_distance: 40
gear_ratio: 80:16
microsteps: 32

##  Make sure to update below for your relevant driver (2208 or 2209)
[tmc2209 stepper_z3]
uart_pin: PE4
interpolate: false
run_current: 0.8
sense_resistor: 0.110
stealthchop_threshold: 0

#####################################################################
#   Bed Heater
#####################################################################

[heater_bed]
##  SSR Pin - HE1
##  Thermistor - TB
heater_pin: PA1
##  Validate the following thermistor type to make sure it is correct
##  Keenovo branded bed heaters should use Generic 3950
##  LDO branded bed heaters will have the sensor type labelled on the heater
##  See https://www.klipper3d.org/Config_Reference.html#common-thermistors for additional options
sensor_type: Generic 3950
sensor_pin: PF3
##  Adjust Max Power so your heater doesn't warp your bed. Rule of thumb is 0.4 watts / cm^2 .
max_power: 0.6
min_temp: 0
max_temp: 120

# #####################################################################
# #   Fan Control
# #####################################################################

[controller_fan controller_fan]
##  Controller fan
pin: PD12
fan_speed: 0.3
kick_start_time: 0.25
idle_timeout: 30
idle_speed: 0.0
shutdown_speed: 0.0
stepper: stepper_x, stepper_y, stepper_z, stepper_z1, stepper_z2, stepper_z3

[controller_fan stepper_fan]
##  Stepper driver fan
pin: PD13
max_power: 0.35
kick_start_time: 0.25
idle_timeout: 30
idle_speed: 0.0
shutdown_speed: 0.0
stepper: stepper_x, stepper_y, stepper_z, stepper_z1, stepper_z2, stepper_z3

#####################################################################
#   LED Control
#####################################################################

[output_pin caselight]
## Chamber Lighting - CNC_FAN5
pin: PD15
pwm:true
shutdown_value: 0
value:0
cycle_time: 0.01

#####################################################################
#   Additional Sensors
#####################################################################

[thermistor CMFB103F3950FANT]
temperature1: 0.0
resistance1: 32116.0
temperature2: 40.0
resistance2: 5309.0
temperature3: 80.0
resistance3: 1228.0

# [temperature_sensor EBBCan]
# sensor_type: temperature_mcu
# sensor_mcu: EBBCan
# min_temp: 0
# max_temp: 100

# # ACCELEROMETER
# [adxl345]
# cs_pin: EBBCan:PB12
# spi_software_sclk_pin: EBBCan:PB10
# spi_software_mosi_pin: EBBCan:PB11
# spi_software_miso_pin: EBBCan:PB2
# axes_map: x,y,z

# [resonance_tester]
# accel_chip: adxl345
# probe_points:
#     # Somewhere slightly above the middle of your print bed
#     147,154, 20

#####################################################################
# 	Homing and Gantry Adjustment Routines
#####################################################################

[idle_timeout]
timeout: 1800

[quad_gantry_level]
##  Use QUAD_GANTRY_LEVEL to level a gantry.
##  Min & Max gantry corners - measure from nozzle at MIN (0,0) and
##  MAX (250, 250), (300,300), or (350,350) depending on your printer size
##  to respective belt positions

##  Gantry Corners for 300mm Build
##  Uncomment for 300mm build
gantry_corners:
   -60,-10
   360,370
##  Probe points
points:
   50,25
   50,225
   250,225
   250,25

#--------------------------------------------------------------------
speed: 100
horizontal_move_z: 10
retries: 5
retry_tolerance: 0.0075
max_adjust: 10

########################################
# EXP1 / EXP2 (display) pins
########################################

[board_pins]
aliases:
    # EXP1 header
    EXP1_1=PE8, EXP1_2=PE7,
    EXP1_3=PE9, EXP1_4=PE10,
    EXP1_5=PE12, EXP1_6=PE13,    # Slot in the socket on this side
    EXP1_7=PE14, EXP1_8=PE15,
    EXP1_9=<GND>, EXP1_10=<5V>,

    # EXP2 header
    EXP2_1=PA6, EXP2_2=PA5,
    EXP2_3=PB1, EXP2_4=PA4,
    EXP2_5=PB2, EXP2_6=PA7,      # Slot in the socket on this side
    EXP2_7=PC15, EXP2_8=<RST>,
    EXP2_9=<GND>, EXP2_10=<5V>

#--------------------------------------------------------------------
[bed_mesh]
speed: 180
# The speed (in mm/s) of non-probing moves during the calibration.
# The default is 50.
horizontal_move_z: 15
# The height (in mm) that the head should be commanded to move to
# just prior to starting a probe operation. The default is 5.
mesh_min: 40,50
# Defines the minimum X, Y coordinate of the mesh for rectangular
# beds. This coordinate is relative to the probe's location. This
# will be the first point probed, nearest to the origin. This
# parameter must be provided for rectangular beds.
mesh_max: 270,270
# Defines the maximum X, Y coordinate of the mesh for rectangular
# beds. Adheres to the same principle as mesh_min, however this will
# be the furthest point probed from the bed's origin. This parameter
# must be provided for rectangular beds.
probe_count: 7,7
fade_start: 1.0
fade_end: 10.0
#move_check_distance: 5.0
# The distance (in mm) along a move to check for split_delta_z.
# This is also the minimum length that a move can be split. Default
# is 5.0.
#mesh_pps: 2, 2
algorithm: bicubic
#bicubic_tension: .2
# deprecated: relative_reference_index: 24

# #####################################################################
# #   Macros
# #####################################################################

[gcode_macro UNSAFE_RAISE_TOOLHEAD_10]
description: Raise toolhead 10mm without homing
gcode:
  G90
  SET_KINEMATIC_POSITION Z=0
  G0 Z10 F600
  M84

#*# <---------------------- SAVE_CONFIG ---------------------->
#*# DO NOT EDIT THIS BLOCK OR BELOW. The contents are auto-generated.
#*#
#*# [heater_bed]
#*# control = pid
#*# pid_kp = 38.723
#*# pid_ki = 1.352
#*# pid_kd = 277.357
#*#
#*# [stepper_z]
#*# position_endstop = -2.563
#*#
#*# [probe]
#*# z_offset = -1.010
#*#
#*# [input_shaper]
#*# shaper_type_x = 2hump_ei
#*# shaper_freq_x = 89.4
#*# shaper_type_y = mzv
#*# shaper_freq_y = 38.2
#*#
#*# [extruder]
#*#
#*# [tool_probe_endstop]
#*# z_offset = -0.6
